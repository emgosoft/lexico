#ifndef LEXICO_H_INCLUDED
#define LEXICO_H_INCLUDED
#include <iostream>

using namespace std;

struct Operador {
    string signo;
    string nombre;
    string tipo;
};

struct Palabra {
    string palabra;
    string funcion;
    string tipo;
};

struct Simbolo_Agrupacion {
    string simbolo_1;
    string simbolo_2;
    string nombre;
};

class Lexico {
    static const int CANT_OPERADORES_ARITMETICOS = 5;
    static const int CANT_OPERADORES_COMPARACION = 9;
    static const int CANT_PALABRAS_RESERVADAS = 28;
    static const int CANT_SIMBOLOS_AGRUPACION = 3;
    static const int TAMANO_MAX_IDENTIFICADOR_VARIABLE = 50;

    Operador operadoresAritmeticos[CANT_OPERADORES_ARITMETICOS];
    Operador operadoresComparacion[CANT_OPERADORES_COMPARACION];
    Palabra palabrasReservadas[CANT_PALABRAS_RESERVADAS];
    Simbolo_Agrupacion simbolosAgrupacion[CANT_SIMBOLOS_AGRUPACION];
public:
    Lexico() {
        //Aritmeticos
        operadoresAritmeticos[0].nombre="Suma";
        operadoresAritmeticos[0].signo="+";
        operadoresAritmeticos[0].tipo="Aritm�tico";
        operadoresAritmeticos[1].nombre="Resta";
        operadoresAritmeticos[1].signo="-";
        operadoresAritmeticos[1].tipo="Aritm�tico";
        operadoresAritmeticos[2].nombre="Multipliaci�n";
        operadoresAritmeticos[2].signo="*";
        operadoresAritmeticos[2].tipo="Aritm�tico";
        operadoresAritmeticos[3].nombre="Divisi�n";
        operadoresAritmeticos[3].signo="/";
        operadoresAritmeticos[3].tipo="Aritm�tico";
        operadoresAritmeticos[4].nombre="M�dulo";
        operadoresAritmeticos[4].signo="%";
        operadoresAritmeticos[4].tipo="Aritm�tico";

        //Comparaci�n o L�gicos
        operadoresComparacion[0].nombre="Menor que";
        operadoresComparacion[0].signo="<";
        operadoresComparacion[0].tipo="Comparaci�n";
        operadoresComparacion[1].nombre="Menor o igual que";
        operadoresComparacion[1].signo="<=";
        operadoresComparacion[1].tipo="Comparaci�n";
        operadoresComparacion[2].nombre="Mayor";
        operadoresComparacion[2].signo=">";
        operadoresComparacion[2].tipo="Comparaci�n";
        operadoresComparacion[3].nombre="Mayor o igual que";
        operadoresComparacion[3].signo=">=";
        operadoresComparacion[3].tipo="Comparaci�n";
        operadoresComparacion[4].nombre="Diferente de";
        operadoresComparacion[4].signo="!=";
        operadoresComparacion[4].tipo="L�gico";
        operadoresComparacion[5].nombre="Igual que";
        operadoresComparacion[5].signo="==";
        operadoresComparacion[5].tipo="Comparaci�n";
        operadoresComparacion[6].nombre="Negaci�n l�gica";
        operadoresComparacion[6].signo="!";
        operadoresComparacion[6].tipo="L�gico";
        operadoresComparacion[7].nombre="AND L�gico";
        operadoresComparacion[7].signo="&&";
        operadoresComparacion[7].tipo="L�gico";
        operadoresComparacion[8].nombre="OR L�gico";
        operadoresComparacion[8].signo="||";
        operadoresComparacion[8].tipo="L�gico";

        //Palabras reservadas
        palabrasReservadas[0].palabra="break";
        palabrasReservadas[0].funcion="Permite salir de un bucle";
        palabrasReservadas[0].tipo="Palabra Reservada";
        palabrasReservadas[1].palabra="if";
        palabrasReservadas[1].funcion="Estructura de control - Condicional";
        palabrasReservadas[1].tipo="Palabra Reservada";
        palabrasReservadas[2].palabra="else";
        palabrasReservadas[2].funcion="Estructura de control - Condicional";
        palabrasReservadas[2].tipo="Palabra Reservada";
        palabrasReservadas[3].palabra="while";
        palabrasReservadas[3].funcion="Ciclo";
        palabrasReservadas[3].tipo="Palabra Reservada";
        palabrasReservadas[4].palabra="int";
        palabrasReservadas[4].funcion="Tipo de Dato";
        palabrasReservadas[4].tipo="Entero";
        palabrasReservadas[5].palabra="bool";
        palabrasReservadas[5].funcion="Tipo de Dato";
        palabrasReservadas[5].tipo="Booleano";
        palabrasReservadas[6].palabra="float";
        palabrasReservadas[6].funcion="Tipo de Dato";
        palabrasReservadas[6].tipo="Flotante";
        palabrasReservadas[7].palabra="string";
        palabrasReservadas[7].funcion="Tipo de Dato";
        palabrasReservadas[7].tipo="Cadena";
        palabrasReservadas[8].palabra="char";
        palabrasReservadas[8].funcion="Tipo de Dato";
        palabrasReservadas[8].tipo="Caracter";
        palabrasReservadas[9].palabra="const";
        palabrasReservadas[9].funcion="Almacenamiento permanente de variable";
        palabrasReservadas[9].tipo="Palabra Reservada";
        palabrasReservadas[10].palabra="class";
        palabrasReservadas[10].funcion="Define una clase";
        palabrasReservadas[10].tipo="Palabra Reservada";
        palabrasReservadas[11].palabra="continue";
        palabrasReservadas[11].funcion="Finaliza un bucle";
        palabrasReservadas[11].tipo="Palabra Reservada";
        palabrasReservadas[12].palabra="delete";
        palabrasReservadas[12].funcion="Liberar memoria din�mica";
        palabrasReservadas[12].tipo="Palabra Reservada";
        palabrasReservadas[13].palabra="new";
        palabrasReservadas[13].funcion="Asigna memoria din�mica";
        palabrasReservadas[13].tipo="Palabra Reservada";
        palabrasReservadas[14].palabra="do";
        palabrasReservadas[14].funcion="Ciclo";
        palabrasReservadas[14].tipo="Palabra Reservada";
        palabrasReservadas[15].palabra="true";
        palabrasReservadas[15].funcion="Variable con valor asignado";
        palabrasReservadas[15].tipo="Palabra Reservada";
        palabrasReservadas[16].palabra="false";
        palabrasReservadas[16].funcion="Variable con valor asignado";
        palabrasReservadas[16].tipo="Palabra Reservada";
        palabrasReservadas[17].palabra="private";
        palabrasReservadas[17].funcion="Declara elemento privado";
        palabrasReservadas[17].tipo="Palabra Reservada";
        palabrasReservadas[18].palabra="public";
        palabrasReservadas[18].funcion="Declara elemento p�blico";
        palabrasReservadas[18].tipo="Palabra Reservada";
        palabrasReservadas[19].palabra="return";
        palabrasReservadas[19].funcion="Indica valor de retorno";
        palabrasReservadas[19].tipo="Palabra Reservada";
        palabrasReservadas[20].palabra="static";
        palabrasReservadas[20].funcion="Variable ubicada estaticamente";
        palabrasReservadas[20].tipo="Palabra Reservada";
        palabrasReservadas[21].palabra="switch";
        palabrasReservadas[21].funcion="Estructura de control condicional";
        palabrasReservadas[21].tipo="Palabra Reservada";
        palabrasReservadas[22].palabra="this";
        palabrasReservadas[22].funcion="Puntero a objeto actual";
        palabrasReservadas[22].tipo="Palabra Reservada";
        palabrasReservadas[23].palabra="void";
        palabrasReservadas[23].funcion="Tipo de Dato";
        palabrasReservadas[23].tipo="Indica no existencia de un valor de retorno";
        palabrasReservadas[24].palabra="case";
        palabrasReservadas[24].funcion="Etiqueta casos de sentencia";
        palabrasReservadas[24].tipo="Palabra Reservada";
        palabrasReservadas[25].palabra="signed";
        palabrasReservadas[25].funcion="Tipo de Dato";
        palabrasReservadas[25].tipo="Entero con signo";
        palabrasReservadas[26].palabra="short";
        palabrasReservadas[26].funcion="Tipo de Dato";
        palabrasReservadas[26].tipo="Entero peque�o";
        palabrasReservadas[27].palabra="for";
        palabrasReservadas[27].funcion="Ciclo";
        palabrasReservadas[27].tipo="Palabra Reservada";

        //Simbolos de agrupacion
        simbolosAgrupacion[0].nombre="Llave";
        simbolosAgrupacion[0].simbolo_1="{";
        simbolosAgrupacion[0].simbolo_2="}";
        simbolosAgrupacion[1].nombre="Corchete";
        simbolosAgrupacion[1].simbolo_1="[";
        simbolosAgrupacion[1].simbolo_2="]";
        simbolosAgrupacion[2].nombre="Par�ntesis";
        simbolosAgrupacion[2].simbolo_1="(";
        simbolosAgrupacion[2].simbolo_2=")";
    }
    void analizarCadena(string cadena) {
        if(!esOperadorAritmetico(cadena)) {
            if(!esOperadorComparacion(cadena)) {
                if(!esSimboloAgrupacion(cadena)) {
                    if(!esPalabraReservada(cadena)) {
                        if(!esInt(cadena)) {
                            if(!esFloat(cadena)) {
                                if(!esIdentificadorVariable(cadena)) {
                                    cout << cadena << " : " << "No existe en el lenguaje";
                                }
                            } else {
                                cout << cadena << " : ";
                                cout << "Es un n�mero flotate";
                            }
                        } else {
                            cout << cadena << " : ";
                            cout << "Es un n�mero entero";
                        }
                    }
                }
            }
        }
    }
    bool esOperadorAritmetico(string caracter) {
        for(int i=0; i<CANT_OPERADORES_ARITMETICOS; i++) {
            if(caracter == operadoresAritmeticos[i].signo) {
                cout << caracter << " : ";
                cout << operadoresAritmeticos[i].nombre;
                cout << " (Operador " << operadoresAritmeticos[i].tipo << ")";
                return true;
            }
        }
        return false;
    }
    bool esOperadorComparacion(string caracter) {
        for(int i=0; i<CANT_OPERADORES_COMPARACION; i++) {
            if(caracter == operadoresComparacion[i].signo) {
                cout << caracter << " : ";
                cout << operadoresComparacion[i].nombre;
                cout << " (Operador " << operadoresComparacion[i].tipo << ")";
                return true;
            }
        }
        return false;
    }
    bool esSimboloAgrupacion(string simbolo) {
        for(int i=0; i<CANT_SIMBOLOS_AGRUPACION; i++) {
            if((simbolo == simbolosAgrupacion[i].simbolo_1) || (simbolo == simbolosAgrupacion[i].simbolo_2)) {
                cout << simbolo << " : " << simbolosAgrupacion[i].nombre << " (S�mbolo de agrupaci�n)";
                return true;
            }
        }
        return false;
    }
    bool esIdentificadorVariable(string identificador) {
        int tamanoVariable = identificador.length();
        if(tamanoVariable > 0) {
            if(tamanoVariable <= TAMANO_MAX_IDENTIFICADOR_VARIABLE) {
                //La identificador debe comenzar con letra o _
                if((isalpha(identificador[0])) || (identificador[0] == '_')) {
                    for(int i=1; i<tamanoVariable; i++) {
                        //Los siguientes deben ser digitos, letras o _
                        if(!isdigit(identificador[i])) {
                            if(!isalpha(identificador[i])) {
                                if(identificador[i] != '_') {
                                    return false;
                                }
                            }
                        }
                    }
                    cout << identificador << " : " << "Puede ser un identificador de variable";
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    bool esPalabraReservada(string palabra) {
        for(int i=0; i<CANT_PALABRAS_RESERVADAS; i++) {
            if(palabra == palabrasReservadas[i].palabra) {
                cout << palabra << " : ";
                cout << palabrasReservadas[i].tipo;
                cout << " (" << palabrasReservadas[i].funcion << ")";
                return true;
            }
        }
        return false;
    }
    bool esInt(string numero) {
        int tamanoNumero = numero.length();
        int posNumero = 0;
        if(tamanoNumero > 0) {
            if(numero[0] == '-') {
                posNumero = 1;
            }
            for(int i=posNumero; i<tamanoNumero; i++) {
                if(!isdigit(numero[i])) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    bool esFloat(string numero) {
        int tamanoNumero = numero.length();
        for(int i=0; i<tamanoNumero; i++) {
            if(numero[i] == '.') {
                if(esInt(numero.substr(0,i))) {
                    if(esInt(numero.substr(i+1,tamanoNumero))) {
                        return true;
                    }
                    return false;
                }
                return false;
            }
        }
        return false;
    }
};

#endif
