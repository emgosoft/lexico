#include <iostream>
#include <cstdlib>
#include "Lexico.h"

using namespace std;

string escanearCadena();

int main() {
    char opc;
    Lexico* lexico = new Lexico();
    bool continuarAnalizando = true;
    setlocale(LC_CTYPE,"Spanish");
    while(continuarAnalizando) {
        lexico->analizarCadena(escanearCadena());
        cout << endl << endl << "\t�Desea continuar analizando cadenas (S/N)? ";
        cin >> opc;
        if((opc == 's') || (opc == 'S')) {
            continuarAnalizando = true;
        } else {
            continuarAnalizando = false;
        }
        cin.get();
    }
    delete lexico;
    return 0;
}

string escanearCadena() {
    string cadena;
    system("CLS");
    cout << endl << "\t\tAnalizador L�xico" << endl;
    cout << endl << "\tIngrese cadena: ";
    getline(cin,cadena);
    cout << endl << "\t";
    return cadena;
}
